package fr.soleil.database.connection.mysql;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

import fr.soleil.database.connection.AbstractDataBaseConnector;

public class MySQLDataBaseConnector extends AbstractDataBaseConnector {

    protected static final String DRIVER_NOT_FOUND = "mysql driver not found ";

    private volatile DataSource dataSource;

    public MySQLDataBaseConnector() {
        super();
        dataSource = null;
    }

    @Override
    public String getDriver() {
        return MySQLConstants.DRIVER_MYSQL;
    }

    @Override
    protected synchronized Connection createConnection() throws SQLException {
        Connection connection = null;
        if (dataSource == null) {
            String url = getDriver() + "://" + getHost() + "/" + getName();
            try {
                Class.forName(MySQLConstants.MYSQL_DRIVER_CLASS).newInstance();
            } catch (final ClassNotFoundException e) {
                // System.err.println(e.getMessage());
                throw new SQLException(DRIVER_NOT_FOUND + e);
            } catch (final InstantiationException e) {
                // System.err.println(e.getMessage());
                throw new SQLException(DRIVER_NOT_FOUND + e);
            } catch (final IllegalAccessException e) {
                // System.err.println(e.getMessage());
                throw new SQLException(DRIVER_NOT_FOUND + e);
            } catch (final Exception e) {
                // System.err.println(e.getMessage());
                throw new SQLException(DRIVER_NOT_FOUND + e);
            }
            GenericObjectPool connectionPool = new GenericObjectPool(null);
            ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(url, getUser(), getPassword());
            new PoolableConnectionFactory(connectionFactory, connectionPool, null, null, false, true);
            connectionPool.setMinIdle(minPoolSize);
            connectionPool.setMaxActive(maxPoolSize);
            connectionPool.setMinEvictableIdleTimeMillis(3000);
            dataSource = new PoolingDataSource(connectionPool);
        }
        if (dataSource != null) {
            connection = dataSource.getConnection();
        }
        return connection;
    }

    @Override
    public MySQLDataBaseConnector clone() {
        return (MySQLDataBaseConnector) super.clone();
    }

}
