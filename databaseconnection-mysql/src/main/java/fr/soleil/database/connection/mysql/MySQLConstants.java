package fr.soleil.database.connection.mysql;

public interface MySQLConstants {

    /**
     * Parameter that represents the MySQL database driver
     */
    public static final String DRIVER_MYSQL = "jdbc:mysql";

    public static final String MYSQL_DRIVER_CLASS = "com.mysql.jdbc.Driver";

}
