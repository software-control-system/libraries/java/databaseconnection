package fr.soleil.database.connection.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.ui.event.ConnectionEvent;
import fr.soleil.database.connection.ui.listener.IConnectionListener;
import fr.soleil.lib.project.application.logging.LogAppender;
import fr.soleil.lib.project.application.logging.LogViewer;

public class DataBaseConnectionTestPanel extends JPanel
        implements DocumentListener, ItemListener, ActionListener, IConnectionListener {

    private static final long serialVersionUID = -8237335873635687683L;

    protected static final Insets DEFAULT_INSETS = new Insets(5, 5, 5, 5);
    protected static final String ORACLE = "Oracle";
    protected static final String MYSQL = "MySQL";

    protected final DataBaseConnectionTester connectionTester;

    protected final JSplitPane splitPane;
    protected final JPanel topPanel;
    protected final JLabel typeLabel;
    protected final JComboBox<AbstractDataBaseConnector> typeComboBox;
    protected final JLabel hostLabel;
    protected final JTextField hostField;
    protected final JLabel nameLabel;
    protected final JTextField nameField;
    protected final JLabel userLabel;
    protected final JTextField userField;
    protected final JLabel passwordLabel;
    protected final JTextField passwordField;
    protected final JLabel dbNameLabel;
    protected final JTextField dbNameField;
    protected final JLabel racLabel;
    protected final JCheckBox racCheckBox;
    protected final JLabel onsConfigLabel;
    protected final JTextField onsConfigField;
    protected final JLabel tnsNameLabel;
    protected final JTextField tnsNameField;
    protected final JButton connectionButton;
    protected final LogViewer logPanel;
    protected final LogAppender logAppender;
    protected final JLabel sqlLabel;
    protected final JTextArea sqlArea;
    protected final JButton sqlButton;

    protected AbstractDataBaseConnector connector;
    protected Connection connection;

    public DataBaseConnectionTestPanel() {
        this(null);
    }

    public DataBaseConnectionTestPanel(DataBaseConnectionTester tester) {
        super(new BorderLayout());
        topPanel = new JPanel(new GridBagLayout());

        this.connectionTester = (tester == null ? new DataBaseConnectionTester() : tester);
        connectionTester.addConnectionListener(this);

        int row = 0;

        typeLabel = createLabel("Database type:");
        typeComboBox = new JComboBox<>(new AbstractDataBaseConnector[] { connectionTester.getOracleConnector(),
                connectionTester.getMySqlConnector() });
        typeComboBox.setRenderer(new ConnectionComboRender());
        typeComboBox.addItemListener(this);
        row = addLabelAndEditor(typeLabel, typeComboBox, row);

        hostLabel = createLabel("Host:");
        hostField = createTextField();
        row = addLabelAndEditor(hostLabel, hostField, row);

        nameLabel = createLabel("Database name:");
        nameField = createTextField();
        row = addLabelAndEditor(nameLabel, nameField, row);

        userLabel = createLabel("User:");
        userField = createTextField();
        row = addLabelAndEditor(userLabel, userField, row);

        passwordLabel = createLabel("Password:");
        passwordField = createTextField();
        row = addLabelAndEditor(passwordLabel, passwordField, row);

        dbNameLabel = createLabel("Database schema:");
        dbNameField = createTextField();
        row = addLabelAndEditor(dbNameLabel, dbNameField, row);

        racLabel = createLabel("Rac:");
        racCheckBox = new JCheckBox();
        racCheckBox.addActionListener(this);
        row = addLabelAndEditor(racLabel, racCheckBox, row);

        onsConfigLabel = createLabel("ONS Configuration:");
        onsConfigField = createTextField();
        row = addLabelAndEditor(onsConfigLabel, onsConfigField, row);

        tnsNameLabel = createLabel("TNS Name:");
        tnsNameField = createTextField();
        row = addLabelAndEditor(tnsNameLabel, tnsNameField, row);

        connectionButton = new JButton("Connect...");
        connectionButton.addActionListener(this);
        GridBagConstraints connectionButtonConstraints = new GridBagConstraints();
        connectionButtonConstraints.fill = GridBagConstraints.BOTH;
        connectionButtonConstraints.gridx = 0;
        connectionButtonConstraints.gridy = row++;
        connectionButtonConstraints.weightx = 1;
        connectionButtonConstraints.weighty = 0;
        connectionButtonConstraints.gridwidth = GridBagConstraints.REMAINDER;
        connectionButtonConstraints.insets = DEFAULT_INSETS;
        topPanel.add(connectionButton, connectionButtonConstraints);

        sqlLabel = createLabel("SQL request:");
        sqlArea = new JTextArea(3, 3);
        sqlButton = new JButton("Run SQL request");
        sqlButton.addActionListener(this);
        sqlButton.setEnabled(false);
        JScrollPane areaScrollPane = new JScrollPane(sqlArea);
        areaScrollPane.setMinimumSize(areaScrollPane.getPreferredSize());
        row = addLabelAndEditor(sqlLabel, areaScrollPane, sqlButton, row, true);

        logPanel = new LogViewer(DataBaseConnectionTestPanel.class.getName());
        logPanel.setBorder(new TitledBorder("Logs"));
        logAppender = new LogAppender(logPanel, false);
        LogAppender.registerAppender(connectionTester.getLogger(), logAppender);

        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitPane.setTopComponent(topPanel);
        splitPane.setBottomComponent(logPanel);
        splitPane.setDividerSize(8);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(0.5);
        splitPane.setResizeWeight(0.5);

        add(splitPane, BorderLayout.CENTER);

        connector = null;
        connection = null;
    }

    protected JLabel createLabel(final String text) {
        JLabel label = new JLabel(text, SwingConstants.RIGHT);
        label.setVerticalAlignment(SwingConstants.CENTER);
        return label;
    }

    protected JTextField createTextField() {
        JTextField textField = new JTextField(20);
        textField.setDragEnabled(true);
        textField.getDocument().addDocumentListener(this);
        return textField;
    }

    protected int addLabelAndEditor(final JLabel label, final JComponent editor, int row) {
        return addLabelAndEditor(label, editor, null, row, false);
    }

    protected int addLabelAndEditor(final JLabel label, final JComponent editor, final JComponent editor2, int row,
            boolean fillHeight) {
        GridBagConstraints labelConstraints = new GridBagConstraints();
        labelConstraints.fill = GridBagConstraints.HORIZONTAL;
        labelConstraints.gridx = 0;
        labelConstraints.gridy = row;
        labelConstraints.weightx = 0;
        labelConstraints.weighty = 0;
        labelConstraints.anchor = GridBagConstraints.CENTER;
        labelConstraints.insets = DEFAULT_INSETS;
        topPanel.add(label, labelConstraints);
        GridBagConstraints editorConstraints = new GridBagConstraints();
        editorConstraints.fill = GridBagConstraints.BOTH;
        editorConstraints.gridx = 1;
        editorConstraints.gridy = row;
        editorConstraints.weightx = 1;
        editorConstraints.weighty = fillHeight ? 1 : 0;
        editorConstraints.insets = DEFAULT_INSETS;
        if (editor2 == null) {
            editorConstraints.gridwidth = GridBagConstraints.REMAINDER;
        }
        topPanel.add(editor, editorConstraints);
        if (editor2 != null) {
            GridBagConstraints editor2Constraints = new GridBagConstraints();
            editor2Constraints.fill = GridBagConstraints.HORIZONTAL;
            editor2Constraints.gridx = 2;
            editor2Constraints.gridy = row;
            editor2Constraints.weightx = 0;
            editor2Constraints.weighty = 0;
            editor2Constraints.anchor = GridBagConstraints.CENTER;
            editor2Constraints.insets = DEFAULT_INSETS;
            topPanel.add(editor2, editor2Constraints);
        }
        return ++row;
    }

    protected void applyText(final DocumentEvent e) {
        if (e != null) {
            if (e.getDocument() == hostField.getDocument()) {
                connectionTester.setDbHost(hostField.getText());
            } else if (e.getDocument() == nameField.getDocument()) {
                connectionTester.setDbName(nameField.getText());
            } else if (e.getDocument() == userField.getDocument()) {
                connectionTester.setDbUser(userField.getText());
            } else if (e.getDocument() == passwordField.getDocument()) {
                connectionTester.setDbPassword(passwordField.getText());
            } else if (e.getDocument() == dbNameField.getDocument()) {
                connectionTester.setDbSchema(dbNameField.getText());
            } else if (e.getDocument() == onsConfigField.getDocument()) {
                connectionTester.setDbOnsConfiguration(onsConfigField.getText());
            } else if (e.getDocument() == tnsNameField.getDocument()) {
                connectionTester.setDbTnsName(tnsNameField.getText());
            }
        }
    }

    @Override
    public void insertUpdate(final DocumentEvent e) {
        applyText(e);
    }

    @Override
    public void removeUpdate(final DocumentEvent e) {
        applyText(e);
    }

    @Override
    public void changedUpdate(final DocumentEvent e) {
        applyText(e);
    }

    @Override
    public void itemStateChanged(final ItemEvent e) {
        if ((e != null) && (e.getStateChange() == ItemEvent.SELECTED) && (e.getSource() == typeComboBox)) {
            boolean visible = (e.getItem() == connectionTester.getOracleConnector());
            racLabel.setVisible(visible);
            racCheckBox.setVisible(visible);
            onsConfigLabel.setVisible(visible);
            onsConfigField.setVisible(visible);
            tnsNameLabel.setVisible(visible);
            tnsNameField.setVisible(visible);
        }
    }

    protected void setElementsEnabled(final boolean enabled) {
        typeComboBox.setEnabled(enabled);
        hostField.setEnabled(enabled);
        nameField.setEnabled(enabled);
        userField.setEnabled(enabled);
        passwordField.setEnabled(enabled);
        dbNameField.setEnabled(enabled);
        onsConfigField.setEnabled(enabled);
        tnsNameField.setEnabled(enabled);
        racCheckBox.setEnabled(enabled);
        connectionButton.setEnabled(enabled);
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        if (e != null) {
            if (e.getSource() == racCheckBox) {
                connectionTester.setDbRac(racCheckBox.isSelected());
            } else if (e.getSource() == connectionButton) {
                setElementsEnabled(false);
                connectionTester.setOracle(typeComboBox.getSelectedItem() == connectionTester.getOracleConnector());
                connectionTester.connect(true);
            } else if (e.getSource() == sqlButton) {
                sqlButton.setEnabled(false);
                connectionTester.setSql(sqlArea.getText().trim());
                connectionTester.runSql(true);
            }
        }
    }

    @Override
    public void connectionChanged(ConnectionEvent event) {
        if ((event != null) && (event.getSource() == connectionTester)) {
            setElementsEnabled(true);
            sqlButton.setEnabled(connectionTester.isConnected() && (!connectionTester.isSqlCanceled()));
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class ConnectionComboRender extends DefaultListCellRenderer {

        private static final long serialVersionUID = -4459587264763547816L;

        @Override
        public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index,
                final boolean isSelected, final boolean cellHasFocus) {
            Object adaptedValue;
            if (value == connectionTester.getOracleConnector()) {
                adaptedValue = ORACLE;
            } else if (value == connectionTester.getMySqlConnector()) {
                adaptedValue = MYSQL;
            } else {
                adaptedValue = value;
            }
            return super.getListCellRendererComponent(list, adaptedValue, index, isSelected, cellHasFocus);
        }
    }

}
