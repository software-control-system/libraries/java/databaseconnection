package fr.soleil.database.connection.ui;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import javax.swing.JFrame;
import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.mysql.MySQLDataBaseConnector;
import fr.soleil.database.connection.oracle.OracleDataBaseConnector;
import fr.soleil.database.connection.ui.event.ConnectionEvent;
import fr.soleil.database.connection.ui.listener.IConnectionListener;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.math.ArrayUtils;

public class DataBaseConnectionTester {

    protected static final Collection<String> NO_UI_ARGUMENTS = Arrays.asList("headless", "noui", "nographics");
    protected static final String TANGO_HOST = "TANGO_HOST";
    protected static final String TANGO_HOST_ARG = "TANGO_HOST=";
    protected static final String EMPTY = "";
    protected static final String EXIT = "exit";
    protected static final Logger LOGGER = LoggerFactory.getLogger(DataBaseConnectionTester.class);

    protected final OracleDataBaseConnector oracleConnector;
    protected final MySQLDataBaseConnector mySqlConnector;

    protected final Collection<IConnectionListener> connectionListeners;

    protected AbstractDataBaseConnector connector;
    protected Connection connection;
    protected String sql;
    protected SQLManager sqlManager;

    public DataBaseConnectionTester() {
        oracleConnector = new OracleDataBaseConnector();
        mySqlConnector = new MySQLDataBaseConnector();
        connectionListeners = Collections.newSetFromMap(new ConcurrentHashMap<>());
        connector = null;
        connection = null;
        sqlManager = null;
    }

    public void addConnectionListener(IConnectionListener listener) {
        if (listener != null) {
            connectionListeners.add(listener);
        }
    }

    public void removeConnectionListener(IConnectionListener listener) {
        if (listener != null) {
            connectionListeners.remove(listener);
        }
    }

    protected void warnConnectionListeners() {
        ConnectionEvent event = new ConnectionEvent(this);
        for (IConnectionListener listener : connectionListeners) {
            if (listener != null) {
                listener.connectionChanged(event);
            }
        }
    }

    public Logger getLogger() {
        return LOGGER;
    }

    public OracleDataBaseConnector getOracleConnector() {
        return oracleConnector;
    }

    public MySQLDataBaseConnector getMySqlConnector() {
        return mySqlConnector;
    }

    public void closeConnection() {
        if (sqlManager != null) {
            sqlManager.cancel();
        }
        if (connector != null) {
            try {
                boolean closed = isClosed(connection);
                connector.closeConnection(connection);
                if (isClosed(connection) && (!closed)) {
                    LOGGER.info("Connection closed.");
                }
            } catch (SQLException e) {
                LOGGER.warn("Failed to close connection", e);
            }
        }
    }

    public boolean isConnected() {
        boolean connected;
        try {
            if (connector == null) {
                connected = false;
            } else {
                connected = connector.isConnected(connection);
            }
        } catch (Exception e) {
            connected = false;
        }
        return connected;
    }

    public void connect(boolean async) {
        LOGGER.info("Trying to connect to selected database...");
        ConnectionMaker maker = new ConnectionMaker(connector);
        maker.connect(async);
    }

    public void runSql(boolean async) {
        sqlManager = new SQLManager();
        sqlManager.runSQL(async);
    }

    protected boolean isClosed(Connection connection) throws SQLException {
        return connection == null ? true : connection.isClosed();
    }

    public void setDbHost(String host) {
        closeConnection();
        oracleConnector.setHost(host);
        mySqlConnector.setHost(host);
    }

    public void setDbName(String name) {
        closeConnection();
        oracleConnector.setName(name);
        mySqlConnector.setName(name);
    }

    public void setDbSchema(String schema) {
        closeConnection();
        oracleConnector.setSchema(schema);
        mySqlConnector.setSchema(schema);
    }

    public void setDbUser(String user) {
        closeConnection();
        oracleConnector.setUser(user);
        mySqlConnector.setUser(user);
    }

    public void setDbPassword(String password) {
        closeConnection();
        oracleConnector.setPassword(password);
        mySqlConnector.setPassword(password);
    }

    public void setDbOnsConfiguration(String onsConfiguration) {
        closeConnection();
        oracleConnector.setOnsConfiguration(onsConfiguration);
    }

    public void setDbTnsName(String tnsName) {
        closeConnection();
        oracleConnector.setTnsName(tnsName);
    }

    public void setDbRac(boolean rac) {
        closeConnection();
        oracleConnector.setRac(rac);
    }

    public void setOracle(boolean oracle) {
        connector = oracle ? oracleConnector : mySqlConnector;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public boolean isSqlCanceled() {
        return sqlManager == null ? false : sqlManager.canceled;
    }

    protected static boolean isNoUIArgument(String arg) {
        boolean noUI = false;
        if (arg != null) {
            arg = arg.trim();
            for (String noUiValue : NO_UI_ARGUMENTS) {
                if (noUiValue.equalsIgnoreCase(arg)) {
                    noUI = true;
                    break;
                }
            }
        }
        return noUI;
    }

    protected static String readInput(Scanner scanner) {
        String input;
        if (scanner == null) {
            input = EMPTY;
        } else {
            try {
                input = scanner.nextLine();
                if (input == null) {
                    input = EMPTY;
                } else {
                    input = input.trim();
                }
            } catch (Exception e) {
                input = EMPTY;
            }
        }
        if (EXIT.equalsIgnoreCase(input)) {
            System.exit(0);
        }
        return input;
    }

    public static void main(final String[] args) {
        boolean noui = GraphicsEnvironment.isHeadless();
        if ((args != null) && (args.length > 0)) {
            for (String arg : args) {
                if (isNoUIArgument(arg)) {
                    noui = true;
                    break;
                } else if ((arg != null) && arg.startsWith(TANGO_HOST_ARG)) {
                    System.setProperty(TANGO_HOST, arg.substring(TANGO_HOST_ARG.length()));
                }
            }
        }
        final DataBaseConnectionTester tester = new DataBaseConnectionTester();
        Runtime.getRuntime().addShutdownHook(new Thread("DataBaseConnectionTester cleaner") {
            @Override
            public void run() {
                OracleDataBaseConnector.clearPoolDataSources();
                tester.closeConnection();
            }
        });
        if (noui) {
            Scanner scanner = null;
            try {
                scanner = new Scanner(System.in);
                System.out.println("Welcome to database connection tester.");
                System.out.println("You can quit at any time, entering 'exit' at input request");
                String yes1 = "y", yes2 = "yes", no1 = "n", no2 = "no";
                String oracle = "oracle", mysql = "mysql";
                String input = EMPTY;
                String dbChoice = "Enter database type (oracle/mySQL): ";
                String name = "Enter database name: ";
                String user = "Enter database user: ";
                String password = "Enter database password: ";
                String schema = "Enter database schema: ";
                // db type
                while ((!oracle.equalsIgnoreCase(input)) && (!mysql.equalsIgnoreCase(input))) {
                    System.out.print(dbChoice);
                    input = readInput(scanner);
                }
                boolean typeOracle = oracle.equalsIgnoreCase(input);
                tester.setOracle(typeOracle);
                input = EMPTY;
                boolean hostMandatory;
                if (typeOracle) {
                    // oracle database might use rac
                    String rac = "Does your database use rac (y/n)?: ";
                    String onsConf = "Enter ONS configuration: ";
                    String tnsNames = "Enter TNS names: ";
                    while ((!yes1.equalsIgnoreCase(input)) && (!yes2.equalsIgnoreCase(input))
                            && (!no1.equalsIgnoreCase(input)) && (!no2.equalsIgnoreCase(input))) {
                        System.out.print(rac);
                        input = readInput(scanner);
                    }
                    // check is rac
                    boolean isRac = yes1.equalsIgnoreCase(input) || yes2.equalsIgnoreCase(input);
                    tester.setDbRac(isRac);
                    input = EMPTY;
                    if (isRac) {
                        hostMandatory = false;
                        // ons configuration
                        while (input.isEmpty()) {
                            System.out.print(onsConf);
                            input = readInput(scanner);
                        }
                        tester.setDbOnsConfiguration(input);
                        input = EMPTY;
                        // tns names
                        while (input.isEmpty()) {
                            System.out.print(tnsNames);
                            input = readInput(scanner);
                        }
                        tester.setDbTnsName(input);
                        input = EMPTY;
                    } else {
                        hostMandatory = true;
                    }
                } else {
                    hostMandatory = true;
                }
                // host
                if (hostMandatory) {
                    while (input.isEmpty()) {
                        String host = "Enter database host: ";
                        System.out.print(host);
                        input = readInput(scanner);
                    }
                    tester.setDbHost(input);
                    input = EMPTY;
                } else {
                    System.out.print("Enter database host (optional, press [Enter] to skip): ");
                    input = readInput(scanner);
                    tester.setDbHost(input);
                    input = EMPTY;
                }
                // name
                while (input.isEmpty()) {
                    System.out.print(name);
                    input = readInput(scanner);
                }
                tester.setDbName(input);
                input = EMPTY;
                // schema
                while (input.isEmpty()) {
                    System.out.print(schema);
                    input = readInput(scanner);
                }
                tester.setDbSchema(input);
                input = EMPTY;
                // user
                while (input.isEmpty()) {
                    System.out.print(user);
                    input = readInput(scanner);
                }
                tester.setDbUser(input);
                input = EMPTY;
                // password
                while (input.isEmpty()) {
                    System.out.print(password);
                    input = readInput(scanner);
                }
                tester.setDbPassword(input);
                input = EMPTY;
                tester.connect(false);
                // SQL
                String separator = "----------------------------";
                String sql = "Enter SQL command: ";
                while (tester.isConnected()) {
                    while (true) {
                        System.out.println(separator);
                        System.out.print(sql);
                        input = readInput(scanner);
                        if (!input.isEmpty()) {
                            tester.setSql(input);
                            tester.runSql(false);
                            System.out.println();
                        }
                    }
                }
            } finally {
                if (scanner != null) {
                    scanner.close();
                }
            }
        } else {
            final DataBaseConnectionTestPanel panel = new DataBaseConnectionTestPanel(tester);
            Rectangle maxBounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
            JFrame testFrame = new JFrame(DataBaseConnectionTester.class.getSimpleName());
            testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            testFrame.setContentPane(panel);
            // big frame for big logs (especially SQL results)
            testFrame.setSize(new Dimension(Math.min(1280, maxBounds.width), Math.min(1024, maxBounds.height)));
            testFrame.setLocationRelativeTo(null);
            testFrame.setVisible(true);
        }

    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class ConnectionMaker {
        protected long time;

        public ConnectionMaker(AbstractDataBaseConnector connector) {
            DataBaseConnectionTester.this.connector = connector;
        }

        public void connect(boolean async) {
            if (async) {
                new ConnectionWorker().execute();
            } else {
                try {
                    ConnectionInitializer initializer = new ConnectionInitializer();
                    initializer.run();
                    ConnectionFinalizer finalizer = new ConnectionFinalizer(initializer.getConnection());
                    finalizer.finalizeConnection();
                } catch (Exception e) {
                    warnConnectionFailed(e);
                }
            }
        }

        protected void warnConnectionFailed(Throwable cause) {
            LOGGER.error("Connection error", cause);
            cause.printStackTrace();
        }

        protected class ConnectionInitializer {
            private Connection connection;

            public void run() throws SQLException {
                connection = null;
                if (connector != null) {
                    time = System.currentTimeMillis();
                    connection = connector.getNewConnection();
                }
            }

            public Connection getConnection() {
                return connection;
            }
        }

        protected class ConnectionFinalizer {
            private final Connection connection;

            public ConnectionFinalizer(Connection connection) {
                super();
                this.connection = connection;
            }

            public void finalizeConnection() {
                time = System.currentTimeMillis() - time;
                DataBaseConnectionTester.this.connection = connection;
                boolean success = false;
                if (connector != null) {
                    success = connector.isConnected(connection);
                }
                if (success) {
                    LOGGER.info(
                            DateUtil.elapsedTimeToStringBuilder(new StringBuilder("Connection successful (it took "),
                                    time, true).append(" to obtain a connection)").toString());
                } else {
                    LOGGER.warn("Connection failed");
                }
                warnConnectionListeners();
            }
        }

        protected class ConnectionWorker extends SwingWorker<Connection, SQLException> {
            protected ConnectionInitializer initializer;

            public ConnectionWorker() {
                super();
                initializer = new ConnectionInitializer();
            }

            @Override
            protected Connection doInBackground() {
                Connection connection = null;
                if (connector != null) {
                    try {
                        initializer.run();
                        connection = initializer.getConnection();
                    } catch (SQLException e) {
                        publish(e);
                    }
                }
                return connection;
            }

            @Override
            protected void process(final List<SQLException> chunks) {
                if (chunks != null) {
                    for (SQLException e : chunks) {
                        LOGGER.error("Connection preparation error", e);
                    }
                }
            }

            @Override
            protected void done() {
                DataBaseConnectionTester.this.connection = null;
                try {
                    Connection connection = get();
                    ConnectionFinalizer finalizer = new ConnectionFinalizer(connection);
                    finalizer.finalizeConnection();
                } catch (InterruptedException e) {
                    LOGGER.warn("Connection interrupted", e);
                } catch (ExecutionException e) {
                    warnConnectionFailed(e.getCause());
                }
            }
        }
    }

    protected class SQLManager {
        protected volatile boolean canceled = false;

        public void cancel() {
            canceled = true;
        }

        public void runSQL(boolean async) {
            if (async) {
                SQLWorker worker = new SQLWorker();
                worker.execute();
            } else {
                SQLExecutor executor = new SQLExecutor();
                try {
                    executor.run();
                } catch (SQLException e) {
                    warnSQLFailed(e);
                }
                SQLFinalizer finalizer = new SQLFinalizer();
                finalizer.finalizeSQL(executor.getResult());
            }
        }

        protected void warnSQLFailed(Throwable cause) {
            String message = "Failed to execute SQL instruction: " + sql;
            if (cause == null) {
                LOGGER.warn(message);
            } else {
                LOGGER.warn(message, cause);
            }
        }

        protected class SQLExecutor {

            private Object result;

            public void run() throws SQLException {
                result = Boolean.FALSE;
                Connection conn = connection;
                if ((conn != null) && (sql != null) && (!sql.isEmpty())) {
                    long execTime = System.currentTimeMillis();
                    PreparedStatement statement = conn.prepareStatement(sql);
                    ResultSet rs = statement.executeQuery();
                    execTime = System.currentTimeMillis() - execTime;
                    if (rs == null) {
                        result = Boolean.TRUE;
                    } else {
                        StringBuilder builder = new StringBuilder("SQL command success! ").append(sql);
                        ResultSetMetaData rsMetaData = rs.getMetaData();
                        if (rsMetaData == null) {
                            DateUtil.elapsedTimeToStringBuilder(builder.append("\nIt took "), execTime)
                                    .append(" to execute the SQL request.");
                        } else {
                            boolean allNull = true;
                            int columnCount = rsMetaData.getColumnCount();
                            for (int i = 1; i <= columnCount; i++) {
                                String label = rsMetaData.getColumnLabel(i);
                                if ((label != null) && (!label.trim().isEmpty())) {
                                    allNull = false;
                                    break;
                                }
                            }
                            if (!allNull) {
                                builder.append("\n");
                                for (int i = 1; i <= columnCount; i++) {
                                    builder.append(String.valueOf(rsMetaData.getColumnLabel(i)).trim());
                                    if (i < columnCount) {
                                        builder.append(", ");
                                    }
                                }
                            }
                            long extractTime = System.currentTimeMillis();
                            boolean limitExceeded = false;
                            int max = 30;
                            int count = 0;
                            while (rs.next()) {
                                builder.append("\n");
                                if (count++ == max) {
                                    builder.append("[...]");
                                    limitExceeded = true;
                                    break;
                                }
                                for (int i = 1; i <= columnCount; i++) {
                                    builder.append(ArrayUtils.toString(rs.getObject(i)));
                                    if (i < columnCount) {
                                        builder.append(", ");
                                    }
                                }
                            }
                            extractTime = System.currentTimeMillis() - extractTime;
                            DateUtil.elapsedTimeToStringBuilder(builder.append("\nIt took "), execTime)
                                    .append(" to execute the SQL request.");
                            DateUtil.elapsedTimeToStringBuilder(builder.append("\nIt took "), extractTime);
                            if (limitExceeded) {
                                builder.append(" to extract the first ").append(max);
                            } else {
                                builder.append(" to extract ").append(count);
                            }
                            builder.append(" lines");
                        }
                        result = builder.toString();
                        rs.close();
                    }
                }
            }

            public Object getResult() {
                return result;
            }
        }

        protected class SQLFinalizer {
            public void finalizeSQL(Object result) {
                String output;
                boolean sqlOk;
                if (result instanceof Boolean) {
                    sqlOk = ((Boolean) result).booleanValue();
                    output = null;
                } else if (result instanceof String) {
                    sqlOk = true;
                    output = (String) result;
                } else {
                    sqlOk = false;
                    output = null;
                }
                if (sqlOk) {
                    LOGGER.info(output);
                } else if (!sql.isEmpty()) {
                    warnSQLFailed(null);
                }
            }
        }

        protected class SQLWorker extends SwingWorker<Object, SQLException> {
            @Override
            protected Object doInBackground() {
                SQLExecutor executor = new SQLExecutor();
                try {
                    executor.run();
                } catch (SQLException e) {
                    publish(e);
                }
                return executor.getResult();
            }

            @Override
            protected void process(final List<SQLException> chunks) {
                if (chunks != null) {
                    for (SQLException e : chunks) {
                        warnSQLFailed(e);
                    }
                }
            }

            @Override
            protected void done() {
                try {
                    Object result = get();
                    SQLFinalizer finalizer = new SQLFinalizer();
                    finalizer.finalizeSQL(result);
                } catch (InterruptedException e) {
                    canceled = true;
                } catch (ExecutionException e) {
                    warnSQLFailed(e.getCause());
                }
                warnConnectionListeners();
            }
        }

    }

}
