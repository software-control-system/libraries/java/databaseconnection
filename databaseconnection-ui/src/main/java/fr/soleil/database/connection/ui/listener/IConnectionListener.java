package fr.soleil.database.connection.ui.listener;

import java.util.EventListener;

import fr.soleil.database.connection.ui.event.ConnectionEvent;

public interface IConnectionListener extends EventListener {

    public void connectionChanged(ConnectionEvent event);

}
