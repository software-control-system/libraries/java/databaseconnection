package fr.soleil.database.connection.ui.event;

import java.util.EventObject;

import fr.soleil.database.connection.ui.DataBaseConnectionTester;

public class ConnectionEvent extends EventObject {

    private static final long serialVersionUID = -3907111378782672364L;

    public ConnectionEvent(DataBaseConnectionTester source) {
        super(source);
    }

    @Override
    public DataBaseConnectionTester getSource() {
        return (DataBaseConnectionTester) super.getSource();
    }

}
