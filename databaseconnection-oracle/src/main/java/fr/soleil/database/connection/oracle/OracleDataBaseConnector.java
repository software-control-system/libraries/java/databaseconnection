package fr.soleil.database.connection.oracle;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.lib.project.ObjectUtils;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

/**
 * {@link AbstractDataBaseConnector} dedicated to Oracle databases
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class OracleDataBaseConnector extends AbstractDataBaseConnector {

    private static final String ALTER_SESSION_SET_NLS_DATE_FORMAT_DD_MM_YYYY_HH24_MI_SS = "alter session set NLS_DATE_FORMAT = 'DD-MM-YYYY HH24:MI:SS'";
    private static final String ALTER_SESSION_SET_NLS_NUMERIC_CHARACTERS = "alter session set NLS_NUMERIC_CHARACTERS = \". \"";
    private static final String ALTER_SESSION_SET_NLS_TIMESTAMP_FORMAT_DD_MM_YYYY_HH24_MI_SS_FF = "alter session set NLS_TIMESTAMP_FORMAT = 'DD-MM-YYYY HH24:MI:SS.FF'";

    private static final ConcurrentMap<String, PoolDataSource> POOL_MAP = new ConcurrentHashMap<>();
    private static final int TIMER_PERIOD = 5000; // 5s
    private static final String TIMER_NAME = new StringBuilder("OracleDataBaseConnector connections cleaner (every ")
            .append(TIMER_PERIOD / 1000).append("s)").toString(); // Timer name
    private static final Timer TIMER;
    static {
        TIMER = new Timer(TIMER_NAME);
        TIMER.schedule(new ConnectionCleanerTask(), 0, TIMER_PERIOD);
    }

    private boolean rac;
    private String onsConfiguration;
    private String tnsName;

    public OracleDataBaseConnector() {
        super();
        rac = false;
        onsConfiguration = null;
        tnsName = null;
    }

    @Override
    public String getDriver() {
        return OracleConstants.DRIVER_ORACLE;
    }

    public boolean isRac() {
        return rac;
    }

    public void setRac(final boolean rac) {
        this.rac = rac;
    }

    public String getOnsConfiguration() {
        return onsConfiguration;
    }

    public void setOnsConfiguration(final String onsConfiguration) {
        if ((onsConfiguration == null) || onsConfiguration.trim().isEmpty()) {
            this.onsConfiguration = null;
        } else {
            String nodes = "nodes=";
            if (onsConfiguration.startsWith(nodes)) {
                this.onsConfiguration = onsConfiguration;
            } else {
                this.onsConfiguration = nodes + onsConfiguration;
            }
        }
    }

    public String getTnsName() {
        return tnsName;
    }

    public void setTnsName(final String tnsName) {
        this.tnsName = tnsName;
    }

    protected String buildUrl() {
        final StringBuilder url = new StringBuilder(getDriver());
        url.append(":@");
        if (rac) {
            url.append(tnsName);
        } else {
            url.append(getHost()).append(":").append(OracleConstants.ORACLE_PORT).append(":").append(getName());
        }
        return url.toString();
    }

    protected StringBuilder urlToStringBuilder(StringBuilder builder) {
        StringBuilder result = builder;
        if (result == null) {
            result = new StringBuilder();
        }
        result.append(getDriver()).append(":@");
        if (rac) {
            result.append(tnsName);
        } else {
            result.append(getHost()).append(":").append(OracleConstants.ORACLE_PORT).append(":").append(getName());
        }
        return result;
    }

    protected StringBuilder appendKeyInformation(StringBuilder builder) {
        StringBuilder result = builder;
        if (result == null) {
            result = new StringBuilder();
        }
        result.append("\nschema:").append(getSchema());
        result.append("\nuser:").append(getUser());
        result.append("\npassword:").append(getPassword());
        return result;
    }

    @Override
    protected synchronized Connection createConnection() throws SQLException {
        Connection connection = null;
        StringBuilder keyBuilder = urlToStringBuilder(null);
        String url = keyBuilder.toString();
        appendKeyInformation(keyBuilder);
        if (!isConnected()) {
            String key = keyBuilder.toString();
            PoolDataSource poolDS = POOL_MAP.get(key);
            if (poolDS == null) {
                PoolDataSource newSource = PoolDataSourceFactory.getPoolDataSource();
                PoolDataSource tmp = POOL_MAP.putIfAbsent(key, newSource);
                if (tmp != null) {
                    boolean closed = false;
                    try {
                        Connection tmpConnect = tmp.getConnection();
                        closed = (tmpConnect == null) || tmpConnect.isClosed();
                    } catch (SQLException e) {
                        closed = true;
                    }
                    if (closed) {
                        POOL_MAP.put(key, newSource);
                    } else {
                        newSource = tmp;
                    }
                }
                poolDS = newSource;
                poolDS.setConnectionFactoryClassName(OracleConstants.ORACLE_DATASOURCE);
                poolDS.setURL(url);
                poolDS.setUser(getUser());
                poolDS.setPassword(getPassword());
                poolDS.setConnectionPoolName(getSchema() + "_UCP_POOL");
                poolDS.setConnectionWaitTimeout(10);
                poolDS.setInactiveConnectionTimeout(inactivityTimeout);
                poolDS.setInitialPoolSize(minPoolSize);
                poolDS.setMinPoolSize(minPoolSize);
                poolDS.setMaxPoolSize(maxPoolSize);
                poolDS.setValidateConnectionOnBorrow(true);
                if (isHarvestable()) {
                    poolDS.setConnectionHarvestTriggerCount(maxPoolSize / 2);
                    poolDS.setConnectionHarvestMaxCount(maxPoolSize / 2);
                }
                if (rac) {
                    poolDS.setONSConfiguration(onsConfiguration);
                    poolDS.setFastConnectionFailoverEnabled(true);
                }
            }
            if (poolDS != null) {
                connection = poolDS.getConnection();
                alterSession(connection);
            }
        }
        return connection;
    }

    // overridden to use synchronization
    @Override
    public synchronized Connection getConnection() {
        return connection;
    }

    /**
     * This method is used when connecting an Oracle database. It tunes the
     * connection to the database.
     * 
     * @throws SQLException
     */
    private void alterSession(final Connection con) throws SQLException {
        if (isConnected(con)) {
            Statement stmt = null;
            try {
                stmt = con.createStatement();
                stmt.executeQuery(ALTER_SESSION_SET_NLS_NUMERIC_CHARACTERS);
                stmt.executeQuery(ALTER_SESSION_SET_NLS_TIMESTAMP_FORMAT_DD_MM_YYYY_HH24_MI_SS_FF);
                stmt.executeQuery(ALTER_SESSION_SET_NLS_DATE_FORMAT_DD_MM_YYYY_HH24_MI_SS);
            } finally {
                if ((stmt != null) && (!stmt.isClosed())) {
                    stmt.close();
                }
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = super.equals(obj);
        if (equals && (obj != this)) {
            OracleDataBaseConnector connector = (OracleDataBaseConnector) obj;
            equals = (rac == connector.rac) && ObjectUtils.sameObject(onsConfiguration, connector.onsConfiguration)
                    && ObjectUtils.sameObject(tnsName, connector.tnsName);
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = super.hashCode();
        int mult = 0x02AC1E;
        code = code * mult + ObjectUtils.generateHashCode(rac);
        code = code * mult + ObjectUtils.getHashCode(onsConfiguration);
        code = code * mult + ObjectUtils.getHashCode(tnsName);
        return code;
    }

    @Override
    public OracleDataBaseConnector clone() {
        return (OracleDataBaseConnector) super.clone();
    }

    /**
     * Clears the pool data source associated with an {@link OracleDataBaseConnector}.
     * <div style="color:#FF0000;font-weight:bold;font-style:italic;text-decoration:underline;font-size: 24px;">
     * Warning!!! Expert usage only!!!
     * </div>
     * 
     * @param connector The {@link OracleDataBaseConnector} for which to clean the pool data source
     */
    public static void clearPoolDataSource(OracleDataBaseConnector connector) {
        if (connector != null) {
            POOL_MAP.remove(connector.urlToStringBuilder(null).toString());
        }
    }

    /**
     * Clears all known pool data sources
     * <div style="color:#FF0000;font-weight:bold;font-style:italic;text-decoration:underline;font-size: 24px;">
     * Warning!!! Expert usage only!!!
     * </div>
     */
    public static void clearPoolDataSources() {
        POOL_MAP.clear();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link TimerTask} to clean POOL_MAP for dead connections
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected static class ConnectionCleanerTask extends TimerTask {
        @Override
        public void run() {
            for (Entry<String, PoolDataSource> entry : POOL_MAP.entrySet()) {
                boolean shouldRemove = false;
                PoolDataSource source = entry.getValue();
                if (source == null) {
                    shouldRemove = true;
                } else {
                    try {
                        Connection connection = source.getConnection();
                        shouldRemove = (connection == null) || connection.isClosed();
                    } catch (SQLException e) {
                        shouldRemove = true;
                    }
                }
                if (shouldRemove) {
                    // remove can be done because POOL_MAP is a ConcurrentHashMap
                    PoolDataSource removed = POOL_MAP.remove(entry.getKey());
                    // restore known connection if it was changed in between
                    if ((removed != null) && (removed != source)) {
                        POOL_MAP.put(entry.getKey(), removed);
                    }
                }
            }
        }
    }

}
