package fr.soleil.database.connection.oracle;

/**
 * Oracle databases useful constants
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface OracleConstants {

    /**
     * Parameter that represents the Oracle database driver
     */
    public static final String DRIVER_ORACLE = "jdbc:oracle:thin";
    /**
     * Port number for the connection
     */
    public static final String ORACLE_PORT = "1521";

    public static final String ORACLE_DATASOURCE = "oracle.jdbc.pool.OracleDataSource";

}
