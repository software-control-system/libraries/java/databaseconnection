package fr.soleil.database.connection;

public interface DBConstants {

    public static final short DEFAULT_MAX_POOL_SIZE = 10;

    public static final short DEFAULT_MIN_POOL_SIZE = 1;

    public static final int DEFAULT_INACTIVITY_TIMEOUT = 1;

}
