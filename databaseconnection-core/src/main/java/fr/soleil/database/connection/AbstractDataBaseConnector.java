package fr.soleil.database.connection;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.SQLException;

import fr.soleil.lib.project.ObjectUtils;

public abstract class AbstractDataBaseConnector implements Cloneable {

    public static final boolean USE_LOG4JDBC = Boolean.parseBoolean(System.getProperty("log4jdbc.active"));

    protected short minPoolSize;
    protected short maxPoolSize;
    protected int inactivityTimeout;
    protected String host;
    protected String name;
    protected String user;
    protected String password;
    protected String schema;
    protected boolean harvestable;
    protected boolean autoCommit;
    protected Connection connection;

    public AbstractDataBaseConnector() {
        minPoolSize = DBConstants.DEFAULT_MIN_POOL_SIZE;
        maxPoolSize = DBConstants.DEFAULT_MAX_POOL_SIZE;
        inactivityTimeout = DBConstants.DEFAULT_INACTIVITY_TIMEOUT;
        harvestable = false;
        autoCommit = true; // new connections are in autocommit mode by default
        connection = null;
    }

    public abstract String getDriver();

    public short getMinPoolSize() {
        return minPoolSize;
    }

    public void setMinPoolSize(final short minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    public short getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(final short maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public int getInactivityTimeout() {
        return inactivityTimeout;
    }

    public void setInactivityTimeout(final int inactivityTimeout) {
        this.inactivityTimeout = inactivityTimeout;
    }

    public String getHost() {
        return host;
    }

    public void setHost(final String host) {
        if ((host == null) || host.trim().isEmpty()) {
            this.host = null;
        } else {
            this.host = host;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(final String dbName) {
        this.schema = dbName;
    }

    public boolean isHarvestable() {
        return harvestable;
    }

    public void setHarvestable(final boolean harvestable) {
        this.harvestable = harvestable;
    }

    public boolean isAutoCommit() {
        return autoCommit;
    }

    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }

    public final void connect() throws SQLException {
        this.connection = getNewConnection();
    }

    protected abstract Connection createConnection() throws SQLException;

    public final Connection getNewConnection() throws SQLException {
        Connection connection = createConnection();
        if (connection != null) {
            try {
                connection.setAutoCommit(autoCommit);
            } catch (SQLException e) {
                try {
                    connection.close();
                } catch (Exception e2) {
                    // Ignore this case
                } finally {
                    connection = null;
                }
                throw e;
            }
        }
        return connection;
    }

    public Connection getConnection() {
        return connection;
    }

    /**
     * Returns this {@link AbstractDataBaseConnector}'s {@link Connection}, with the possibility to automatically
     * initialize it.
     * 
     * @param autoReconnect Whether to automatically initialize the {@link ConnectException}. If <code>true</code>, and
     *            the {@link Connection} is <code>null</code> or closed, then it will be initialized.
     * @return A {@link Connection}
     * @throws SQLException If a problem occurred.
     */
    public Connection getConnection(boolean autoReconnect) throws SQLException {
        if (autoReconnect) {
            if (!isConnected()) {
                connect();
            }
        }
        return getConnection();
    }

    public boolean isConnected() {
        boolean result;
        Connection connection = this.connection;
        result = isConnected(connection);
        if (!result) {
            connection = null;
        }
        this.connection = connection;
        return result;
    }

    public boolean isConnected(Connection connection) {
        boolean result;
        if (connection == null) {
            result = false;
        } else {
            try {
                result = !connection.isClosed();
            } catch (SQLException e) {
                // Database access impossible --> the connection is closed or lost
                result = false;
            }
            if (result) {
                try {
                    result = connection.isValid(0);
                } catch (SQLException e) {
                    // The connection is not valid --> consider as not connected
                    result = false;
                }
                if (!result) {
                    try {
                        connection.close();
                    } catch (SQLException e1) {
                        // Nothing to do: either the connection is already closed, or it was lost.
                    }
                }
            }
        }
        return result;
    }

    public void closeConnection() throws SQLException {
        closeConnection(getConnection());
    }

    public void closeConnection(final Connection conn) throws SQLException {
        closeConnection(conn, false);
    }

    public void closeConnection(final Connection conn, boolean forceAutoCommit) throws SQLException {
        if ((conn != null) && (!conn.isClosed())) {
            if (forceAutoCommit) {
                try {
                    if (!conn.getAutoCommit()) {
                        try {
                            conn.commit();
                        } catch (SQLException e) {
                            // Ignore this exception, for now
                        }
                        conn.setAutoCommit(true);
                    }
                } catch (SQLException e) {
                    // Ignore this exception, for now
                }
            }
            conn.close();
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            AbstractDataBaseConnector connector = (AbstractDataBaseConnector) obj;
            equals = (minPoolSize == connector.minPoolSize) && (maxPoolSize == connector.maxPoolSize)
                    && (inactivityTimeout == connector.inactivityTimeout) && (harvestable == connector.harvestable)
                    && (autoCommit == connector.autoCommit) && ObjectUtils.sameObject(host, connector.host)
                    && ObjectUtils.sameObject(name, connector.name) && ObjectUtils.sameObject(user, connector.user)
                    && ObjectUtils.sameObject(password, connector.password)
                    && ObjectUtils.sameObject(schema, connector.schema);

        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xDA7A;
        int mult = 0xBA5E;
        code = code * mult + getClass().hashCode();
        code = code * mult + minPoolSize;
        code = code * mult + maxPoolSize;
        code = code * mult + inactivityTimeout;
        code = code * mult + ObjectUtils.generateHashCode(harvestable);
        code = code * mult + ObjectUtils.generateHashCode(autoCommit);
        code = code * mult + ObjectUtils.getHashCode(host);
        code = code * mult + ObjectUtils.getHashCode(name);
        code = code * mult + ObjectUtils.getHashCode(user);
        code = code * mult + ObjectUtils.getHashCode(password);
        code = code * mult + ObjectUtils.getHashCode(schema);
        return code;
    }

    @Override
    public AbstractDataBaseConnector clone() {
        AbstractDataBaseConnector clone;
        try {
            clone = (AbstractDataBaseConnector) super.clone();
            clone.connection = null;
        } catch (CloneNotSupportedException e) {
            // Should not happen as AbstractDataBaseConnector implements Cloneable
            clone = null;
        }
        return clone;
    }

}
